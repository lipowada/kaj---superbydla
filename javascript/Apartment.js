// trida Byt obsahuje nazev bytu, seznam spolubydlicih a seznam dluhu, ktere bytu nalezi
class Apartment {
    constructor(name, roommates, debts) {
        this._name = name;
        this._roommates = roommates;
        this._debts = debts;
    }

    getName() {
        return this._name;
    }

    getRoommates() {
        let array = [];
        this._roommates.map(mate => array.push(User.retrieveUser(mate)));
        return array;
    }

    getDebts() {
        let array = [];
        this._debts.map(debt => {
            array.push(Debt.retrieveDebt(debt));
        });
        return array;
    }

    setName(name) {
        this._name = name;
    }

    addRoommate(roommate) {
        this._roommates.push(roommate.getUsername());
    }

    addDebt(debt) {
        this._debts.push(debt.getName());
    }

    removeDebt(debt) {
        this._debts.splice(this._debts.indexOf(debt.getName()), 1);
        localStorage.removeItem(debt.getName());
        localStorage.setItem(this._name, JSON.stringify(this));
    }

    // ziskani uzivatele z uloziste
    static retrieveApartment(apartment) {
        const data = JSON.parse(localStorage.getItem(apartment));
        if (data === null) return null;
        return new Apartment(data._name, data._roommates, data._debts);
    }

    // vytvoreni a ulozeni bytu do uloziste
    static createNewApartment(name){
        const apartment = new Apartment(name, [], []);
        localStorage.setItem(apartment.getName(), JSON.stringify(apartment));
        return apartment;
    }
}