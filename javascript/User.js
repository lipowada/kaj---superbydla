// trida Uzivatel obsahuje detaily uzivatele
class User {
    constructor(username, password, apartment) {
        this._username = username;
        this._password = password;
        this._apartment = apartment;
    }

    getUsername() {
        return this._username;
    }

    getPassword() {
        return this._password;
    }

    getApartment() {
        return Apartment.retrieveApartment(this._apartment);
    }

    setApartment(apartment) {
        this._apartment = apartment.getName();
    }

    // zmeni heslo uzivatele a presmeruje uzivatele na posledni adresu
    changePassword(newPassword) {
        this._password = newPassword;
        localStorage.setItem(this._username, JSON.stringify(this));
        history.back();
    }

    // vrati true, pokud se heslo uzivatele rovna defaultnimu heslu "admin"
    hasDefaultPassword(){
        return this._password === "admin";
    }

    // vrati uzivatele z uloziste
    static retrieveUser(user) {
        const data = JSON.parse(localStorage.getItem(user));
        if (data === null) return null;
        return new User(data._username, data._password, data._apartment);
    }

    // vytvori a ulozi uzivatele do uloziste
    static createNewUser(username, password, apartment){
        const user = new User(username, password, apartment.getName());
        apartment.addRoommate(user);
        localStorage.setItem(apartment.getName(), JSON.stringify(apartment));
        localStorage.setItem(user.getUsername(), JSON.stringify(user));
        return user;
    }

}