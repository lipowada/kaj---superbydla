// trida se seznamem chybovych hlasek pro js validaci formularu
class Msg {
    static loginEmptyUsername (){
        return "loginEmptyUsername";
    }

    static loginEmptyPassword(){
        return "loginEmptyPassword";
    }

    static loginInvalidUsername(){
        return "loginInvalidUsername";
    }

    static loginInvalidPassword(){
        return "loginInvalidPassword";
    }

    static apartmentEmptyUsername(){
        return "apartmentEmptyUsername";
    }

    static apartmentEmptyPassword(){
        return "apartmentEmptyPassword";
    }

    static apartmentEmptyPasswordAgain(){
        return "apartmentEmptyPasswordAgain";
    }

    static apartmentEmptyName(){
        return "apartmentEmptyName";
    }

    static apartmentPasswordsInvalid(){
        return "apartmentPasswordsInvalid";
    }

    static apartmentTakenUsername(){
        return "apartmentTakenUsername";
    }

    static apartmentTakenName(){
        return "apartmentTakenName";
    }

}