// trida FormHandler upravuje formulare pri validaci a poskytuje tak uzivateli udaje o chybach ve vyplnovani
class FormHandler {

    // nastavi vzhled policek formulare na validni
    static resetForm(name){
        switch (name) {
            case "login": FormHandler.resetLogin(); break;
            case "apartment": FormHandler.resetApartment(); break;
            default:  break;
        }
    }

    // provadi zmeny formulare
    static changeForm(msg){
        switch (msg) {
            case "loginEmptyUsername": FormHandler.loginEmptyUsername(); break;
            case "loginEmptyPassword": FormHandler.loginEmptyPassword(); break;
            case "loginInvalidUsername": FormHandler.loginInvalidUsername(); break;
            case "loginInvalidPassword": FormHandler.loginInvalidPassword(); break;
            case "apartmentEmptyUsername": FormHandler.apartmentEmptyUsername(); break;
            case "apartmentEmptyPassword": FormHandler.apartmentEmptyPassword(); break;
            case "apartmentEmptyPasswordAgain": FormHandler.apartmentEmptyPasswordAgain(); break;
            case "apartmentEmptyName": FormHandler.apartmentEmptyName(); break;
            case "apartmentPasswordsInvalid": FormHandler.apartmentPasswordsInvalid(); break;
            case "apartmentTakenUsername": FormHandler.apartmentTakenUsername(); break;
            case "apartmentTakenName": FormHandler.apartmentTakenName(); break;
            default:  break;
        }
    }

    static loginEmptyUsername(){
        const input = document.getElementById("formLoginUsername");
        input.classList.add("inputNameInvalid");

        const targetEl = document.querySelector('.msgUsername');
        targetEl.innerHTML = `prazdne uzivatelske jmeno`;
    }

    static loginEmptyPassword(){
        const input = document.getElementById("formLoginPassword");
        input.classList.add("inputPasswordInvalid");

        const targetEl = document.querySelector('.msgPassword');
        targetEl.innerHTML = `prazdne heslo`;
    }

    static loginInvalidUsername(){
        const input = document.getElementById("formLoginUsername");
        input.classList.add("inputNameInvalid");

        const targetEl = document.querySelector('.msgUsername');
        targetEl.innerHTML = `uzivatelske jmeno neexistuje`;
    }

    static loginInvalidPassword(){
        const input = document.getElementById("formLoginPassword");
        input.classList.add("inputPasswordInvalid");

        const targetEl = document.querySelector('.msgPassword');
        targetEl.innerHTML = `nespravne heslo`;
    }

    static apartmentEmptyUsername(){
        const input = document.getElementById("formFirstRoommateName");
        input.classList.add("inputNameInvalid");

        const targetEl = document.querySelector('.msgUsername');
        targetEl.innerHTML = `prazdne uzivatelske jmeno`;
    }

    static apartmentEmptyPassword(){
        const input = document.getElementById("formFirstRoommatePassword");
        input.classList.add("inputPasswordInvalid");

        const targetEl = document.querySelector('.msgPassword');
        targetEl.innerHTML = `prazdne heslo`;
    }

    static apartmentEmptyPasswordAgain(){
        const input = document.getElementById("formFirstRoommatePasswordAgain");
        input.classList.add("inputPasswordInvalid");

        const targetEl = document.querySelector('.msgPasswordAgain');
        targetEl.innerHTML = `prazdne opakovane heslo`;
    }

    static apartmentEmptyName(){
        const input = document.getElementById("formApartmentName");
        input.classList.add("inputNameInvalid");

        const targetEl = document.querySelector('.msgApartmentName');
        targetEl.innerHTML = `prazdny nazev bytu`;
    }

    static apartmentPasswordsInvalid(){
        const psw2 = document.getElementById("formFirstRoommatePasswordAgain");
        psw2.classList.add("inputPasswordInvalid");

        const targetEl = document.querySelector('.msgPasswordAgain');
        targetEl.innerHTML = `puvodni heslo se neshoduje se zopakovanym heslem`;
    }

    static apartmentTakenUsername(){
        const input = document.getElementById("formFirstRoommateName");
        input.classList.add("inputNameInvalid");

        const targetEl = document.querySelector('.msgUsername');
        targetEl.innerHTML = `uzivatelske jmeno je jiz zabrane`;
    }

    static apartmentTakenName(){
        const input = document.getElementById("formApartmentName");
        input.classList.add("inputNameInvalid");

        const targetEl = document.querySelector('.msgApartmentName');
        targetEl.innerHTML = `Nazev bytu je jiz zabrany`;
    }


    static resetLogin(){
        const username = document.getElementById("formLoginUsername");
        username.classList.remove("inputNameInvalid");
        const password = document.getElementById("formLoginPassword");
        password.classList.remove("inputPasswordInvalid");

        document.querySelector('.msgUsername').innerHTML =``;
        document.querySelector('.msgPassword').innerHTML =``;
    }

    static resetApartment(){
        const username = document.getElementById("formFirstRoommateName");
        username.classList.remove("inputNameInvalid");
        const password = document.getElementById("formFirstRoommatePassword");
        password.classList.remove("inputPasswordInvalid");
        const password2 = document.getElementById("formFirstRoommatePasswordAgain");
        password2.classList.remove("inputPasswordInvalid");
        const apartmentName = document.getElementById("formApartmentName");
        apartmentName.classList.remove("inputNameInvalid");


        document.querySelector('.msgUsername').innerHTML =``;
        document.querySelector('.msgPassword').innerHTML =``;
        document.querySelector('.msgPasswordAgain').innerHTML =``;
        document.querySelector('.msgApartmentName').innerHTML =``;
    }
}