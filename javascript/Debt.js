// trida Dluh obsahuje podrobnosti dluhu
class Debt {
    constructor(name, owner, borrowers, checklist, figure, postDate, deadline) {
        this._name = name;
        this._owner = owner;
        this._borrowers = borrowers;
        this._checkList = checklist;
        this._figure = figure;
        this._postDate = postDate;
        this._deadline = deadline;
    }

    getName() {
        return this._name;
    }

    getOwner() {
        return User.retrieveUser(this._owner);
    }

    getBorrowers() {
        let array = [];
        this._borrowers.map(mate => array.push(User.retrieveUser(mate)));
        return array;
    }

    getCheckList() {
        return this._checkList;
    }

    getFigure() {
        return this._figure
    }

    getPostDate() {
        return  new MyDate(this._postDate._day, this._postDate._month);
    }

    getDeadline() {
        return new MyDate(this._deadline._day, this._deadline._month);
    }

    setName(name) {
        this._name = name;
    }

    setOwner(owner) {
        this._owner = owner.getUsername();
    }

    addBorrower(borrower) {
        this._borrowers.push(borrower.getUsername());
    }

    setCheckList(checkList) {
        this._checkList = checkList;
    }

    setPaid(index) {
        this._checkList[index] = true;
    }

    setNotPaid(index) {
        this._checkList[index] = false;
    }


    setFigure(figure) {
        this._figure = figure
    }

    setPostDate(postDate) {
        this._postDate = postDate
    }

    setDeadline(deadline) {
        this._deadline = deadline
    }

    // vrati true, pokud je dany uzivatel dluznik
    isBorrower(user){
        // musi mi prijit cela instance tridy
        return this._borrowers.includes(user.getUsername());
    }

    // nastavi uzivateli stav zaplaceno
    setUserPayed(user) {
        const index = this._borrowers.indexOf(user.getUsername());
        this._checkList[index] = true;
        localStorage.setItem(this._name, JSON.stringify(this));
    }

    // vrati true, pokud dany uzivatel dluh jiz zaplatil
    hasPayed(user) {
        return this._checkList[this._borrowers.indexOf(user.getUsername())];
    }

    // vrati true, pokud je dluh po deadlinu
    isOverdue(){
        const date = this.getDeadline();
        return date.isLessThan(MyDate.getCurrentDate());
    }

    // vytahne dluh z uloziste
    static retrieveDebt(debt) {
        const data = JSON.parse(localStorage.getItem(debt));
        if (data === null) return null;
        return new Debt(data._name, data._owner, data._borrowers, data._checkList, data._figure, data._postDate, data._deadline);
    }
}
