// kontroluje validitu formulare pro prihlaseni a tvorbu noveho bytu
// komunikuje s FormHandler
class FormValidator {
    // zkontroluje login
    static validateLogin(){
        const username = document.getElementById("formLoginUsername").value.trim();
        const password = document.getElementById("formLoginPassword").value.trim();
        FormHandler.resetForm("login");
        if (username.length === 0) {
            FormHandler.changeForm(Msg.loginEmptyUsername());
        }

        if (password.length === 0) {
            FormHandler.changeForm(Msg.loginEmptyPassword());
        }

        const retrievedItem = User.retrieveUser(username);
        if (username.length !== 0 && retrievedItem === null) {
            FormHandler.changeForm(Msg.loginInvalidUsername());
            return false;
        }

        if (retrievedItem !== null && password === retrievedItem.getPassword()) {
            return retrievedItem;
        } else if (retrievedItem !== null && password !== retrievedItem.getPassword()) {
            FormHandler.changeForm(Msg.loginInvalidPassword());
            return false;
        } else {return false;}

    }

    // zkontroluje formular na novy byt
    static validateApartment(){
        const username = document.getElementById("formFirstRoommateName").value.trim();
        const password = document.getElementById("formFirstRoommatePassword").value.trim();
        const password2 = document.getElementById("formFirstRoommatePasswordAgain").value.trim();
        const apartmentName = document.getElementById("formApartmentName").value.trim();
        FormHandler.resetForm("apartment");
        let valid = true;

        if (username.length === 0) {
            console.log("Empty username");
            FormHandler.changeForm(Msg.apartmentEmptyUsername());
            valid = false;
        }

        if (password.length === 0) {
            console.log("Empty password");
            FormHandler.changeForm(Msg.apartmentEmptyPassword());
            valid = false;
        }

        if (password2.length === 0) {
            console.log("Empty passwordAgain");
            FormHandler.changeForm(Msg.apartmentEmptyPasswordAgain());
            valid = false;
        }

        if (apartmentName.length === 0) {
            console.log("Empty apartment name");
            FormHandler.changeForm(Msg.apartmentEmptyName());
            valid = false;
        }

        if (password.length !== 0 && password2.length !== 0 && password !== password2) {
            console.log("Passwords do not match");
            FormHandler.changeForm(Msg.apartmentPasswordsInvalid());
            valid = false;
        }

        const retrievedUser = User.retrieveUser(username);
        if (username.length !== 0 && retrievedUser !== null) {
            console.log("Username taken");
            FormHandler.changeForm(Msg.apartmentTakenUsername());
            valid = false;
        }

        const retrievedApartment = Apartment.retrieveApartment(apartmentName);
        if (apartmentName.length !== 0 && retrievedApartment !== null) {
            console.log("Apartment name taken");
            FormHandler.changeForm(Msg.apartmentTakenName());
            valid = false;
        }

        return valid;
    }
}