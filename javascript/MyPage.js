// trida MyPage ma na starost vytvareni cele stranky, spoluprace s Router
class MyPage{
    constructor(){
        userNs.createUserOverview();
        sideNs.createSideBar();
        MyPage.changePage();

        window.addEventListener('popstate', e => {
            MyPage.changePage()
        });
    }

    // metodu vola Router, zarizuje vytvareni pozadovane casti stranky
    static changePage(){
        const hash = window.location.hash;
        if (rs.getLoggedUser() === null && hash !== "#createApartment" && hash !== "#loginPage") {
            console.log("nikdo neni prihlaseny, probiha presmerovani na loginPge");
            history.pushState(null, null, "#loginPage");
            loginNs.createLoginPage();
            return;
        }
        const title = "Superbydla";

        switch (hash) {
            case "#loginPage": loginNs.createLoginPage(); break;
            case "#debtsOverview": ns.createDebtsOverview(); break;
            case "#createDebt": ns.createAddDebt(); break;
            case "#roommatesOverview": roommateNs.createRoommatesOverview(); break;
            case "#createRoommate": roommateNs.createAddRoommate(); break;
            case "#createApartment": loginNs.createAddNewApartment(); break;
            case "#changePassword": userNs.createChangePassword(); break;
            case "#logout": userNs.logout(); break;
            default: Router.login(); break;
        }
        document.title = title + window.location.hash;
    }
}