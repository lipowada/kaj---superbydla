// slouzi k routovani
class Router {

    // hlavni metoda, ktera provede routing na zaklade daneho hashe
    static route (hash){
        history.pushState(null, null, "#" + hash);
        // let state = {test: "test"};
        // history.pushState(state, hash, "#" + hash);
        MyPage.changePage();
    };

    static login (){
        this.route("loginPage");
    };

    static debts (){
        this.route("debtsOverview");
    };

    static createDebt (){
        this.route("createDebt");
    };

    static roommates (){
        this.route("roommatesOverview");
    };

    static createRoommate (){
        this.route("createRoommate");
    };

    static createApartment (){
        this.route("createApartment");
    };

    static changePassword (){
        this.route("changePassword")
    }

    static back(){
        history.back();
    }
}
