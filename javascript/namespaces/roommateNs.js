// namespace obsahuje metody pro sekci Lide
const roommateNs = new function () {

    // nacte data z formulare a vytvori nove spolubydlu
    function createNewRoommateFromForm(apartment) {
        const name = document.getElementById("formRoommateName").value;
        const password = "admin";

        const roommate = new User(name, password, apartment.getName());
        localStorage.setItem(roommate.getUsername(), JSON.stringify(roommate));
        apartment.addRoommate(roommate);
        localStorage.setItem(apartment.getName(), JSON.stringify(apartment));

        Router.roommates();
    }

    // vytvori formular pro vytvoreni noveho spolubydly
    this.createAddRoommate = function () {
        const targetEl = document.querySelector('.mainContent');
        let apartment = rs.getApartment();

        targetEl.innerHTML = `
                <div class="windowAddRoommate form-background">
                <h2>Pridat spolubydlici/ho</h2>
                <form id="addRoommateForm">
                <label for="formRoommateName">Jmeno</label>
                <input type="text" id="formRoommateName" name="formRoommateName" placeholder="karel" required autofocus maxlength="100" class="form-text-box"><br>
                <input type="text" id="formRoommateApartment" hidden name="apartment" value="${apartment.getName()}"><br>
                <button id="formRoommateButton" class="form-button">Pridat</button>
                <button type="button" id="backButton" class="back-button" onclick="Router.back()">Zpet</button>
                </form>
                </div>
        `;

        const formInput = document.querySelector("#addRoommateForm");
        formInput.addEventListener("submit", () => createNewRoommateFromForm(apartment));
    };

    // vytvori seznam spolubydlicich
    const createRoommatesList = function (apartment, user, targetEl) {
        let loggedUser = user;
        targetEl.innerHTML = `
        <div class="form-background">
        <h2>Vasi spolubydlici</h2>
        <ul class="tableRoommates">
        </ul>
        </div>

        `;

        const component = apartment.getRoommates().filter(mate => mate.getUsername() !== loggedUser.getUsername()).map(mate => {
            const html = `
            <li>${mate.getUsername()}</li>
            `;
            return html;
        });
        let tableBody = document.querySelector('.tableRoommates');
        tableBody.innerHTML += component.join('');
    };

    // vytvori prehled sekce Lide
    this.createRoommatesOverview = function () {
        const targetEl = document.querySelector('.mainContent');
        let loggedUser = rs.getLoggedUser();
        let apartment = rs.getApartment();

        targetEl.innerHTML = `
        <div id="addRoommate"><span> <i class="fas fa-plus-circle"></i>Pridat spolubydlu</span></div>
        <div class="windowRoommates"></div>
        `;
        const addRoommateInput = document.querySelector("#addRoommate");
        addRoommateInput.addEventListener("click", () => {
            Router.createRoommate();
        });
        createRoommatesList(apartment, loggedUser, document.querySelector('.windowRoommates'));
    };
};
