// namespace s metodama pro login
const loginNs = new function () {

    // provede ukony spoejen s prihlasenim, presmeruje uzivatele na stranku s dluhy
    const onLoginTasks = function(user){
        rs.setLoggedUser(user);
        rs.setApartment(user.getApartment());
        sideNs.createSideBar();
        userNs.createUserOverview();
        if (user.hasDefaultPassword()) {
            alert("musite si zmenit heslo");
            userNs.createChangePassword(user, document.querySelector('.mainContent'));
        } else {
            // ns.createDebtsOverview();
            Router.debts();
        }
    };

    // nacte data z formulare a vytvori noveho uzivatele a novy byt
    const createNewApartmentFromForm = function() {
        const username = document.getElementById("formFirstRoommateName").value;
        const password = document.getElementById("formFirstRoommatePassword").value;
        const apartmentName = document.getElementById("formApartmentName").value;

        // vytvorit byt
        const apartment = Apartment.createNewApartment(apartmentName);

        const roommate = User.createNewUser(username, password, apartment);
        onLoginTasks(roommate);
    };

    // vytvori formular pro vytvoreni noveho uzivatele a noveho bytu
    this.createAddNewApartment = function () {
        const targetEl = document.querySelector('.mainContent');
        targetEl.innerHTML = `
            <div class="windowNewApartment">
            <h2>Tvorba noveho bytu</h2>
            <form>
            <label for="formFirstRoommateName">Vase uzivatelske jmeno:</label>
            <input type="text" id="formFirstRoommateName" name="formFirstRoommateName" placeholder="uzivatel1" autofocus maxlength="100" class="form-text-box">
            <span class="msgUsername"></span><br>
            <label for="formFirstRoommatePassword">Vase heslo:</label>
            <input type="password" id="formFirstRoommatePassword" name="formFirstRoommatePassword" maxlength="100" class="form-text-box">
            <span class="msgPassword"></span><br>
            <label for="formFirstRoommatePasswordAgain">Vase heslo znovu:</label>
            <input type="password" id="formFirstRoommatePasswordAgain" name="formFirstRoommatePasswordAgain" maxlength="100" class="form-text-box">
            <span class="msgPasswordAgain"></span><br>
            <label for="formApartmentName">Nazev bytu:</label>
            <input type="text" id="formApartmentName" name="formApartmentName" placeholder="byt u Novaku" maxlength="100" class="form-text-box">
            <span class="msgApartmentName"></span><br>
            <button type="button" id="formApartmentButton" class="form-button">Pridat</button><br>
            <button type="button" id="backToLoginButton" class="back-button">Zpet</button>
            </form>
            </div>
            `;
        const newApartmentInput = document.querySelector("#formApartmentButton");
        newApartmentInput.addEventListener("click", () =>{
            const result = FormValidator.validateApartment();
            if (result !== false) createNewApartmentFromForm();
        });

        const backInput = document.querySelector("#backToLoginButton");
        backInput.addEventListener("click", () => Router.back());
    };

    // vytvori login stranku
    this.createLoginPage = function () {

        const targetEl = document.querySelector('.mainContent');

        targetEl.innerHTML = `
        <div class="windowLogin">
        <div class="headline">SuperBydla</div>
        <div class="login-headline">
        <h2>Prihlaseni</h2>
        </div>
        
        <div class="windowLoginCredentials">
        <form class="form-login">
        <label for="formLoginUsername" class="label-text">Uzivatelske jmeno</label>
        <input type="text" id="formLoginUsername" name="formLoginUsername" value="petr" placeholder="uzivatel1" autofocus maxlength="100" class="form-text-box">
        <span class="msgUsername"></span><br>
        <label for="formLoginPassword" class="label-text">Heslo</label>
        <input type="password" id="formLoginPassword" name="formLoginPassword" value="psw" maxlength="100" class="form-text-box">
        <span class="msgPassword"></span><br>
        <button type="button" id="formLoginButton" class="form-button2">Prihlasit</button>
        </form>
        
                <br>
        Chcete vytvorit byt? Kliknete <span class="ref-apartment"><a href="#createApartment">zde</a>.</span>
        <br>
        Nemáte ještě účet? Některý z vašich spolubydlících jej pro vás musí založit.
        </div>
        </div>

        `;
        const loginButtonInput = document.querySelector("#formLoginButton");
        loginButtonInput.addEventListener("click", () => {
            const result = FormValidator.validateLogin();
            if (result !== false) onLoginTasks(result);
        });
    };


};
