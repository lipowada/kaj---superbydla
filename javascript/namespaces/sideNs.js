// namespace co obsahuje metody pro vytvareni sidebaru
const sideNs = new function () {

    // vytvori seznam dluhu s nedalekym deadlinem
    const createDeadlinesOverview = function (apartment, user, targetEl) {
        const upcomingDebts = apartment.getDebts().filter(debt =>{
            return (debt.isBorrower(user)) && (debt.getDeadline().closeToCurrentDate() === true)
        });

        if (upcomingDebts.length === 0){
            targetEl.innerHTML += `
                <div>Nemate zadne blizici se zavazky</div>
                `;
        } else {
            const component =  upcomingDebts.map( debt =>{
                const html = `
                    <div class="side-debts">${debt.getName()} - ${debt.getDeadline().toString()}</div>
                    `;
                return html;
            });
            targetEl.innerHTML += component.join('');
        }
    };

    // vytvori sidebar
    this.createSideBar = function () {
        const targetEl = document.querySelector('.sideContent');
        let loggedUser = rs.getLoggedUser();
        let apartment = rs.getApartment();

        if (loggedUser === null) return;

        targetEl.innerHTML = `
        <h2 id="addDebt2" class="side-headline">Nejblizsi zavazky</h2>
        <div class="windowSideBar"></div>
        `;

        createDeadlinesOverview(apartment, loggedUser, document.querySelector('.windowSideBar'));
    }
};
