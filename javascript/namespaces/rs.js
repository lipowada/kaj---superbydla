// Resources
// uloziste (krome local storage) dat prihlaseneho uzivatele a daneho bytu
const rs = new function () {
    // prihlaseny uzivatel
    let loggedUser = (localStorage.loggedUser && JSON.parse(localStorage.loggedUser) !== null) ? User.retrieveUser(JSON.parse(localStorage.loggedUser)._username) : null;

    //byt, ke kteremu prihlaseny uzivatel patri
    let apartment = (localStorage.loggedApartment != null && JSON.parse(localStorage.loggedApartment) !== null) ? Apartment.retrieveApartment(JSON.parse(localStorage.loggedApartment)._name) : null;

    this.getLoggedUser = function () {
        return loggedUser;
    };

    this.setLoggedUser = function (user) {
        localStorage.loggedUser = JSON.stringify(user);
        loggedUser = user;
    };

    this.getApartment = function () {
        return apartment;
    };

    this.setApartment = function (apart) {
        localStorage.loggedApartment = JSON.stringify(apart);
        apartment = apart;
    };

};
