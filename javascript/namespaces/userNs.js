// namespace, ktery ma metody pro vytvareni prehledu uzivatele v hlavicce, zmenu hesla odhlaseni
const userNs = new function () {

    // vytvori formular pro zmenu hesla
    this.createChangePassword = function () {
        const targetEl = document.querySelector('.mainContent');
        let user = rs.getLoggedUser();
        targetEl.innerHTML = `
            <h2>Zmena hesla</h2>
            <div class="form-background">
            <form id="changePasswordForm">
                <label for="formCurrentPassword">Aktualni heslo</label>
                <input type="password" id="formCurrentPassword" name="formCurrentPassword" required autofocus maxlength="100" class="form-text-box"><br>
                <label for="formNewPassword">Nove heslo</label>
                <input type="password" id="formNewPassword" name="formNewPassword" required maxlength="100" class="form-text-box"><br>
                <label for="formNewPasswordAgain">Nove heslo znovu</label>
                <input type="password" id="formNewPasswordAgain" name="formNewPasswordAgain" required maxlength="100" class="form-text-box"><br>
                <input type="text" id="formPasswordChangeUser" hidden name="user" value="${user.getUsername()}"><br>
                <button id="formNewPasswordChangeButton" class="form-button">Pridat</button>
                <button type="button" id="backButton" onclick="Router.back()" class="back-button">Zpet</button>
                </form>
                </div>
            `;

        const formInput = document.querySelector("#changePasswordForm");
        formInput.addEventListener("submit", () => user.changePassword(document.getElementById("formNewPassword").value));
    };

    // odhlasi uzivatele a presmeruje na login
    this.logout = function () {
        rs.setLoggedUser(null);
        rs.setApartment(null);
        Router.login();
    };

    // vytvori prehled uzivatele v headru
    this.createUserOverview = function () {
        const targetEl = document.querySelector('.userNav');
        let loggedUser = rs.getLoggedUser();
        if (loggedUser === null) return;

        targetEl.innerHTML = `
            <span>Odhlasit se: ${loggedUser.getUsername()}</span>
            `;
    }
};
