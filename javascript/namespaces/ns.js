// hlavni namespace
// obsahuje metody pro prehled dluhu
const ns = new function () {
    // prida funkcionalitu na odstraneni pohledavky tlacitkum v tabulce
    const initializeReceivablesActionButton = function (apartment, debts) {
        debts.map(debt => {
            if (debt.getCheckList().every(boolean => boolean === true)) {
                document.getElementById(debt.getName()).addEventListener("click", () => {
                    apartment.removeDebt(debt);
                    ns.createDebtsOverview();
                })
            }
        });
    };

    // prida funkcionalitu pro oznameni splaceni zavazku tlacitkum v tabulce
    function initializeObligationsPayButton(apartment, debts, user) {
        debts.map(debt => {
            if (!debt.hasPayed(user)) {
                document.getElementById(debt.getName()).addEventListener("click", () => {
                    debt.setUserPayed(user);
                    ns.createDebtsOverview();
                });
            }
        });
    }

    // nacte data z fomulare a vytvori novou pohledavku
    const createNewDebtFromForm = function (apartment) {
        const name = document.getElementById("formDebtName").value;
        const borrowers = [];
        const checkList = [];
        const figure = parseInt(document.getElementById("formDebtFigure").value, 10);
        const day = parseInt(document.getElementById("formDebtDay").value, 10);
        const month = parseInt(document.getElementById("formDebtMonth").value, 10);
        const deadline = new MyDate(day, month);
        const currentDate = MyDate.getCurrentDate();
        const owner = document.getElementById("formDebtOwner").value;
        const borrowersData = document.querySelectorAll("input[type=checkbox]");

        Object.values(borrowersData)
            .filter(u => u.checked === true)
            .map(u => {
                borrowers.push(u.value);
            });

        for (let i = 0; i < borrowers.length; i++) {
            checkList[i] = false;
        }
        // name, owner, borrowers, checklist, figure, postDate, deadline

        const debt = new Debt(name, owner, borrowers, checkList, figure, currentDate, deadline);
        localStorage.setItem(name, JSON.stringify(debt));

        // TODO pridat ten dluh i do bytu, jinak se to nebude zobrazovat

        apartment.addDebt(debt);
        localStorage.setItem(apartment.getName(), JSON.stringify(apartment));

        // ns.createDebtsOverview();
        Router.debts();
    };

    // vytvori formular pro pridani nove pohledavky
    this.createAddDebt = function () {
        const targetEl = document.querySelector('.mainContent');
        let loggedUser = rs.getLoggedUser();
        let apartment = rs.getApartment();

        let list = apartment.getRoommates().filter(mate => mate.getUsername() !== loggedUser.getUsername()).map(mate => {
            const html = `
            <label>
                <input type="checkbox" name="${mate.getUsername()}" value="${mate.getUsername()}"> ${mate.getUsername()} <br>
                </label>
                `;
            return html;
        });

        if (list.length === 0){
            list[0] = `MUSITE NEJPRVE PRIDAT SPOLUBYDLICI! ZADNI NEEXISTUJOU! <br>`;
        }

        // TODO Opravit: ten hidden input na vlastnika mi dela radek navic, ktery asi nechci, poresit nejak
        targetEl.innerHTML = `
        <h2>Pridavani pohledavky</h2>
                <div class="windowsAddDebt form-background">
                <form id="addDebtForm">
                <label for="formDebtName">Nazev</label>
                <input type="text" id="formDebtName" name="formDebtName" placeholder="talire" required autofocus maxlength="100" class="form-text-box"><br>
<!--                <label for="borrowers" id="formDebtBorrowers">Osoby pro pridani pohledavky:<br>-->
                Osoby pro pridani pohledavky:<br>
                ${list.join('')}
                </label>
                <label for="formDebtFigure">Castka</label>
                <input type="text" id="formDebtFigure" name="formDebtFigure" required pattern="[0-9]+" maxlength="100" placeholder="1000" class="form-text-box"><br>
                <label for="formDebtDay">Datum splatnosti:<br> Den</label>
                <input type="text" id="formDebtDay" name="formDebtDay" pattern="[0-9]{1,2}" placeholder="12" required class="form-text-box"><br>
                <label for="formDebtMonth">Mesic</label>
                <input type="text" id="formDebtMonth" name="formDebtMonth" required pattern="[0-9]{1,2}" placeholder="7" class="form-text-box"><br>
                <input type="text" id="formDebtOwner" hidden name="owner" value="${loggedUser.getUsername()}"><br>
<!--                <button type="button" id="formDebtButton" class="form-button">Pridat</button>-->
                <button id="formDebtButton" class="form-button">Pridat</button>
                <button type="button" id="backButton" class="back-button" onclick="Router.back()">Zpet</button>
                </form>
                </div>
        `;
        // const buttonInput = document.querySelector("#formDebtButton");
        // buttonInput.addEventListener("click", () => createNewDebtFromForm(apartment));

        const formSubmit = document.querySelector("#addDebtForm");
        formSubmit.addEventListener("submit", () => createNewDebtFromForm(apartment));


    };

    // vytvori seznam vsech pohledavek uzivatele
    const createReceivablesOverview = function (apartment, user, targetEl) {
        const debtList = apartment.getDebts()
            .filter(debt => debt.getOwner().getUsername() === user.getUsername());
        if (debtList.length === 0) {
            targetEl.innerHTML = `
            <h2>Pohledavky</h2>
            <div>nemate zadne pohledavky</div>
            `;
            return;
        }

        targetEl.innerHTML = `
        <h2>Pohledavky</h2>
        <table class="debt-table table-receivables">
        <thead>
        <tr>
        <th>nazev</th>
        <th>vlastnik</th>
        <th>dluzitele</th>
        <th>cena celkove</th>
        <th>cena na osobu</th>
        <th>pridano</th>
        <th>deadline</th>
        <th>akce</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
        `;


        const component = debtList
            .map(debt => {
                const list = debt.getBorrowers().map(u => {
                    if (debt.hasPayed(u)) {
                        const html = `
                    <span class="checkedUserObligation">${u.getUsername()}</span><br>
                    `;
                        return html;
                    } else {
                        const html = `
                    ${u.getUsername()}<br>
                    `;
                        return html;
                    }
                });

                const pricePerPerson = Math.round((debt.getFigure()) / (debt.getBorrowers().length + 1));

                let actionButton = null;
                if (debt.getCheckList().every(boolean => boolean === true)) {
                    actionButton = `
                    <span class="delete-debt" id="${debt.getName()}">Odstranit pohledavku</span>
                    `;
                } else {
                    actionButton = `
                    <span>Nesplaceno</span>
                    `;
                }

                const className = debt.isOverdue() ? "debtOverdue" : "debtNotOverdue" ;


                const html = `
                <tr>
                <td>${debt.getName()}</td>
                <td>${debt.getOwner().getUsername()}</td>
                <td>${list.join('')}</td>
                <td>${debt.getFigure()}</td>
                <td>${pricePerPerson}</td>
                <td>${debt._postDate._day}. ${debt._postDate._month}.</td>
                <td class=${className}>${debt._deadline._day}. ${debt._deadline._month}.</td>
                <td>${actionButton}</td>
                </tr>
           `;
                return html;
            });
        let tableBody = document.querySelector('.table-receivables');
        tableBody.innerHTML += component.join('');
        initializeReceivablesActionButton(apartment, debtList);
    };


    //  vytvori seznam vsech zavazku uzivatele
    const createObligationsOverview = function (apartment, user, targetEl) {
        targetEl.innerHTML = '';
        const debtList = apartment.getDebts()
            .filter(debt => debt.getBorrowers().some(u => u.getUsername() === user.getUsername()));
        if (debtList.length === 0) {
            targetEl.innerHTML = `
            <h2>Zavazky</h2>
            <div>nemate zadne zavazky</div>
            `;
            return;
        }
        const component = debtList
            .map(debt => {
                const list = debt.getBorrowers().map(u => {
                    if (debt.hasPayed(u)) {
                        const html = `
                    <span class="checkedUserObligation">${u.getUsername()}</span><br>
                    `;
                        return html;
                    } else {
                        const html = `
                    ${u.getUsername()}<br>
                    `;
                        return html;
                    }

                });

                let payButton = null;
                if (debt.hasPayed(user)) {
                    payButton = `
                    <span class="payForDebt buttonAlreadyPayed" id="${debt.getName()}">Zaplaceno</span>
                    `
                } else {
                    payButton = `
                    <span class="payForDebt" id="${debt.getName()}">Oznamit zaplaceni</span>
                    `
                }

                const pricePerPerson = Math.round((debt.getFigure()) / (debt.getBorrowers().length + 1));

                const className = debt.isOverdue() ? "debtOverdue" : "debtNotOverdue" ;

                const html = `
                <tr>
                <td>${debt.getName()}</td>
                <td>${debt.getOwner().getUsername()}</td>
                <td>${list.join('')}</td>
                <td>${debt.getFigure()}</td>
                <td>${pricePerPerson}</td>
                <td>${debt._postDate._day}. ${debt._postDate._month}.</td>
                <td class=${className}>${debt._deadline._day}. ${debt._deadline._month}.</td>
                <td>${payButton}</td>
</tr>
           `;

                return html;
            });
        targetEl.innerHTML = `
        <h2>Zavazky</h2>
        <table class="debt-table table-obligations">
        <thead>
        <tr>
        <th>nazev</th>
        <th>vlastnik</th>
        <th>dluzitele</th>
        <th>cena celkove</th>
        <th>cena na osobu</th>
        <th>pridano</th>
        <th>deadline</th>
        <th>akce</th>
        </tr>
        </thead>
        <tbody >
        </tbody>
        </table>
        `;
        let tableBody = document.querySelector('.table-obligations');
        tableBody.innerHTML += component.join('');
        initializeObligationsPayButton(apartment, debtList, user);
    };


    // vytvori odkaz na pridani pohledavky a nadpisy pohledavky a zavazky
    this.createDebtsOverview = function () {
        const targetEl = document.querySelector('.mainContent');
        let loggedUser = rs.getLoggedUser();
        let apartment = rs.getApartment();

        targetEl.innerHTML = `
        <div id="addDebt"><span> <i class="fas fa-plus-circle"></i>Pridat pohledavku</span></div>
        <div class="windowReceivables"></div>
        <div class="windowObligations"></div>
        `;
        let addDebtInput = document.querySelector("#addDebt");
        addDebtInput.addEventListener("click", () => {
            Router.createDebt();
        });


        createReceivablesOverview(apartment, loggedUser, document.querySelector('.windowReceivables'));
        createObligationsOverview(apartment, loggedUser, document.querySelector('.windowObligations'))
    };
};
