// trida Kocka slouzi jako audio cast stranky - umi mnoukat
// kdykoliv se nekdo ze spolubydlu bude citi smutny, muze si na kocce zamnoukat (kliknutim na logo webu Spolubydla) pro rozveseleni
class Cat{
    constructor(){
        this._sound = new Audio("animal_cat_meow.mp3");

        const imgs = document.getElementsByTagName("img");
        console.log(imgs);

        for (let i = 0; i < imgs.length; i++) {
            imgs[i].addEventListener("click", () => this.meow());
        }
    }

    // metoda se stara o simulaci zvukoveho projevu Kocky Domaci (Felis silvestris f. catus) z podceledi male kocky
    meow(){
        this._sound.play();
    }
}