// trida MyDate slouzi jako simulace funkcionality kalendare
// datum je dvojice den - mesic
class MyDate {
    constructor(day, month) {
        this._day = day;
        this._month = month;
    }

    getDay() {
        return this._day
    }

    getMonth() {
        return this._month
    }

    // dotaz zda je this. mensi nez (date)
    isLessThan(date) {
        if (this._month < date._month) {
            return true;
        } else if (this._month === date._month) {
            return this._day < date._day;
        }
        return false;

    }

    // vrati true, pokud se deadline .this blizi deadlinu dateToCompare
    closeTo(dateToCompare) {
        if (this.isLessThan(dateToCompare)) return false;
        if (this._month === dateToCompare._month) {
            console.log("dnesek " + dateToCompare._day);
            console.log("datum splatky "+this._day);
            return ((dateToCompare._day + 5) >= this._day);
        } else {
            return ((this._month - dateToCompare._month) === 1) && (((dateToCompare._day - this._day)) >= 25);
        }
    };

    // vrati true, pokud se deadline .this blizi dnesnimu dni
    closeToCurrentDate() {
        // return (()=> this.closeTo(MyDate.getCurrentDate()));
        return this.closeTo(MyDate.getCurrentDate());
    }

    // vrati instanci MyDate nastavenou na dnesni den
    static getCurrentDate() {
        const date = new Date();
        return new MyDate(date.getDate(), date.getMonth() + 1);
    }

    toString(){
        return this._day + "." +this._month + ".";
    }
}
